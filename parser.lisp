(defpackage #:dialog-parser
  (:use #:cl
        #:alexandria
        #:split-sequence)
  (:export #:parse-dialog
           #:parse-dialog-file))
(in-package #:dialog-parser)

(defvar *whitespaces*
  '(#\Space #\Newline #\Backspace #\Tab 
    #\Linefeed #\Page #\Return #\Rubout))

(defun whitespacep (char)
  (member char *whitespaces*))

(defun parse-dialog (string &optional (*current* "") (*previous* ""))
  (declare (special *current* *previous*))
  (let ((first *current*))
    `(,@(parse-dialog* string)
        ,@(unless (string= first *current*)
            `((".set" ,first ,*current*))))))

(defun reset-current ()
  (declare (special *current* *previous*))
  (rotatef *current* *previous*)
  (unless (string= *current* "")
    `((".set" ,*current* ,*previous*))))

(defun set-current (name)
  (declare (special *current* *previous*))
  (setf *previous* *current*
        *current* name))

(defun parse-dialog* (string)
  (declare (special *current* *previous*))
  (let (in-string)
    (multiple-value-bind (char pos)
        (dolist (char '(#\; #\. #\" #\,) nil)
          (let ((pos 0))
            (map nil (lambda (schar)
                       (when (and (char= char schar) (not in-string))
                         (return (values char pos)))
                       (incf pos)
                       (when (char= #\" schar)
                         (setf in-string (not in-string))))
                 string)))
      ;;(prl char pos)
      (case char
        (#\;
         (let* ((front (parse-dialog* (subseq string 0 pos)))
                (comment (subseq string (1+ pos)))
                (pos (position #\Newline comment))
                (rest (when pos (parse-dialog* (subseq comment (1+ pos))))))
           `(,@front
             ,@rest)))
        (#\.
         (let ((front (parse-dialog* (subseq string 0 pos)))
               (rest (parse-dialog* (subseq string (1+ pos)))))
           `(,@front
             ,@rest)))
        (#\"
         (let* ((front (parse-dialog* (subseq string 0 pos)))
                (rest (subseq string (1+ pos))))
           (unless front
             (setf front
                   (reset-current)))
           (let ((pos (position #\" rest)))
             (unless pos (error "String not finished"))
             (let ((text (subseq rest 0 pos))
                   (next (subseq rest (1+ pos))))
               `(,@front
                 ,@(if (string= *current* "")
                       (error "Noone talking")
                       (mappend (lambda (line)
                                  (let ((line (string-trim *whitespaces*
                                                           (car (split-sequence #\; line)))))
                                    (unless (string= line "")
                                      (list
                                       `(".say" ,*current* ,line)))))
                                (split-sequence #\Newline  text)))
                 ,@(parse-dialog* next))))))
        (#\,
         (let* ((front (parse-dialog* (subseq string 0 pos)))
                (string (subseq string (1+ pos)))
                (args (delete "" (split-sequence-if #'whitespacep string) :test #'string= )))
         `(,@front
           ,@(if (string= *current* "")
                 (error "Noone doing")
                 (when args `((,(car args) ,*current* ,@(cdr args))) )))))
        #+nil
        (#\Newline
         (let ((first (subseq string 0 pos))
               (rest (subseq string (1+ pos))))
           (let* ((first (parse-dialog first current-name previous-name))
                  (name (cadar first)))
             (when name
               (setf previous-name current-name
                     current-name name))
             `(,@first ,@(parse-dialog rest current-name previous-name)))))
        ((nil)
         (let* ((args (delete "" (split-sequence-if #'whitespacep string) :test #'string= )))
           (when args
             `(,@(let ((str (car args)))
                   (unless (or (string= str "") (string= str *current*))
                     (set-current str)
                     `((".set" ,*current* ,*previous*))))
                 ,@(when (cdr args)
                     `((,(cadr args) ,*current* ,@(cddr args))))))))
        (t (error "Character ~a not implemented yet" char))))))

(defun get-file (filename)
  (with-open-file (stream filename)
    (format nil "~{~a~%~}"
            (loop for line = (read-line stream nil)
               while line
               collect line))))

(defun parse-dialog-file (filename)
  (parse-dialog (get-file filename)))
