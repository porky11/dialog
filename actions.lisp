(defpackage #:dialog-actions
  (:use #:cl #:alexandria)
  (:export
   #:get-action
   #:set-action

   #:call-action
   #:call-dialog

   #:with-dialog-context
   #:with-new-dialog-context

   #:defaction))
(in-package #:dialog-actions)

(defvar *actions*)

(defun get-action (id)
  (ensure-gethash id *actions*))

(defun set-action (id value)
  (setf (gethash id *actions*) value))

(defun call-action (arguments)
  (if-let ((fn (get-action (car arguments))))
    (apply fn (cdr arguments))
    (warn "Undefined action ~a" (car arguments))))

(defun call-dialog (actions)
  (dolist (a actions)
    (call-dialog a)))

(defmacro defaction (id arguments &body body)
  `(set-action ,id
               ,(if (consp arguments)
                    `(lambda ,arguments ,@body)
                    (if (stringp arguments)
                        `(lambda (&rest arguments)
                           (call-action (cons ,arguments arguments))
                           ,@(mapcar (lambda (arg)
                                       (unless (stringp arg)
                                         (error "Invalid call of defaction"))
                                       `(call-action (cons ,arg arguments)))
                                     body))
                        (error "Invalid call of defaction")))))

(defmacro with-dialog-context (context &body body)
  `(let ((*actions* ,context))
     ,@body
     *actions*))

(defmacro with-new-dialog-context (&body body)
  `(with-dialog-context (make-hash-table :test #'equal)
     ,@body))

