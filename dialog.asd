(asdf:defsystem #:dialog
  :author "Fabio Krapohl <fabio.u.krapohl@fau.de>"
  :description "Convert dialogs to lists and call them in reversable ways"
  :license "LLGPL"
  :depends-on (#:alexandria #:split-sequence)
  :components ((:file "parser")
               (:file "actions")))


