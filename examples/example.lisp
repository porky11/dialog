(defpackage #:dialog-example
  (:use #:cl
        #:dialog-parser
        #:dialog-actions))
(in-package #:dialog-example)

(defun show-image (name)
  (declare (ignore name))
  ;;show new image
  )

(defun hide-image (name)
  (declare (ignore name))
  ;;hide some image
  )

(defvar *actions*)

(defun call-next ()
  (when *actions*
    (call-action (pop *actions*))))

(with-new-dialog-context
  (defaction ".say" (who what)
    (format t "~a: ~S" who what))
  (defaction ".set" (new old)
    (hide-image old)
    (show-image new)
    (call-next))
  (defaction "go" (who where)
    (format t "~a goes to ~a." who where))
  (defaction "feel" (who &rest how)
    (format t "~a feels ~{~a~^ and ~}!" who how))
  (defaction "answer" (who)
    (loop
       for line = (progn
                    (format t "Answer: ")
                    (read-line))
       if (not (string= line ""))
       do (progn
            (format t "~a answers: ~S" who line)
            (return))))

  (let ((*actions* (parse-dialog-file (format nil "~aexamples/example.dialog" (ql:where-is-system :dialog)))))
    (loop while *actions*
       do (call-next)
       do (read-line))))

  
  
