Dialog
======

Convert dialogs to a list of list and call them as functions.

You can write something in form of the dialog language.

In order to test it, just load the example `example.lisp` in directory `examples` using `(load "examples/example.lisp").

# Language

The language consists of sentences and literal speech.

Comments cover the whole line after a `;` outside of literal speech.

Sentences will trigger actions, literal speech will trigger a special action.


## Sentences

Sentences look like this:
```
actor action arguments
;;example
Hans go from A to B
```
The actor here is `"Hans"` action is `"go"` and the arguments are `"from" "A" "to" "B"`.

In order to write multiple Sentences, you have to end a sentence with a `.`
```
Hans go.
Fritz go.
```
Between both sentences the actor changes, so the special Action `".set"` is called with two arguments, the new actor `"Fritz"` and the old actor `"Hans"`.

When the same person does multiple things, `".set"` is not called:
```
Hans go.
Hans walk.
```
When a person does multiple things, it can be seperated by `,`. Then the actor does not have to be written. The previous example can be written like this:
```
Hans go, walk.
```

## Literal Speech

Another special action is used for literal speech. Therefore you have to write some said sentence between two `"`. Before you can write, who says something.
```
Hans "Hello"
```

Then the special action `".say"` is called with the arguments `"Hans"` and `"Hello"`
After literal speech it's not required to have a `.` to end a sentence. You just can start with a new actor:
```
Hans "Hallo"
Hans go
```

You can also use `,` again to describe the same:
```
Hans "Hallo", go
```

When there is a bigger dialog, it can be written this way:
```
Hans "Hello"
Fritz "Hey"
Hans "How are you?"
Fritz "I'm fine"
```

When the actor is alternating it's possible to drop the name in such cases. After literal speech or `.` the actor is implicitely set to the previous actor:
```
Hans "Hello"
Fritz "Hey"
"How are you?"
"I'm fine"
```

This way it's even possible to never write the name of the person when inside a dialog:
```
Hans visit Fritz.
Fritz is happy.
"Hello"
"Hey"
"How are you?"
"I'm fine"
```

If some literal speech contains newlines, they are converted to multiple actions:
```
Hans "Hi
How are you?
I'm fine"
;; same as
Hans "Hi"
Hans "How are you?"
Hans "I'm fine"
```
Empty lines are omitted.

Comments are even possible inside strings. They either end, when the line ends or when the string ends:
```
Hans "Hi ;comment
How are you? ;other comment" ;not comment by default
```

# Interface

The parser is defined in the package `#:dialog-parser`.
There are the functions `#'parse-dialog`, which takes a string, and `#'parse-dialog-file`, which takes the path to a textfile.
Both return lists of specific actions.

In order to use these actions, there is the package `#:dialog-actions`.


_Macro_

`with-new-dialog-context &body body`

Executes `body` inside a new dialog context. Returns this context afterwards for reusing.

_Macro_

`with-dialog-context context &body body`

Executes `body` inside the dialog context `context`. Returns this context afterwards for reusing.

Following functions and macros need to becalled inside a dialog context:

_Function_

`get-action id`

Returns the action function associated with the identifier `id`.

_Function_

`set-action id value`

Sets the action function associated with the identifier `id` to value.

_Function_

`call-action action`

Calls an action as returned by the parser. The format of this is a list, whose first element is the identifier of the action.

_Function_

`call-dialog dialog`

Calls the dialog as returned by the parser. This is done by calling `#'call-action` for all elements of the dialog.

_Macro_

`defaction name lambda-list &body body`

`name`: a string as identifier of a new action.

`lambda-list`: a lambda list for the action function.

`body`: the function body.

Defines a new action like a function.

_Macro_

`defaction &rest ids`

Defines a new action as combination of multiple actions identified by `ids` represended by strings, which all take the same arguments.

All action functions have to take at least two arguments. The first argument is the calling direction. When calling with other values, it represents calling backwards.

In order to call a dialog, you would just iterate over the list returned by some parsing function, and call `#'call-action` on it. For simplified, but less useful usage, just call `#'call-dialog`.






